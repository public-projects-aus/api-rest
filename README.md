# NodeJS - API-Rest

# Requisitos
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [MongoDB](https://www.mongodb.com/)
- [Postman](https://www.getpostman.com/)

# Installation

Run the following commands on a terminal:

```
git clone  https://gitlab.com/public-projects-aus/api-rest.git
```

```
cd api-rest
```

#Install
```
npm install express
```
```
npm install @types/express @types/node ts-node typescript -D
```

#Install MongoDB
```
sudo apt update
```
```
sudo apt install mongodb
```

#Install MongoDB from MongoDB repo
```
$ sudo apt update && sudo apt install gnupg -y
```
```
$  wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```
```
$ echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/
mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
```
```
sudo apt update
```
```
 sudo apt install mongodb-org
```

#Using MongoDB
```
 mongo
```

# To run the project

Use the command: 

```
npm i
```

```
npm start
```

# Browser or Postman

Open the following route that is specified in server.ts:

```
localhost:3000/
```

EXEMPLE POSTMAM REQUEST

GET 
```
localhost:3000/api/vehicle/
```

    "result": [
        {
            "revisoes": {
                "dataRevisao": "18.12.2020",
                "valor": "$100"
            },
            "_id": "5fdf90327159cb066cc023e3",
            "placa": "HER",
            "marca": "FUSCA",
            "modelo": "TP20",
            "cor": "BRANCO",
            "anofabricacao": "2020",
            "createdAt": "2020-12-20T17:56:02.835Z",
            "__v": 0
        }
    ]
}


POST
```
localhost:3000/api/vehicle/
```


    "result": {
        "revisoes": {
            "dataRevisao": "18.12.2020",
            "valor": "$100"
        },
        "_id": "5fdf90327159cb066cc023e3",
        "placa": "HER",
        "marca": "FUSCA",
        "modelo": "TP20",
        "cor": "BRANCO",
        "anofabricacao": "2020",
        "createdAt": "2020-12-20T17:56:02.835Z",
        "__v": 0
    }
}

DELETE
```
localhost:3000/api/vehicle/ID
```

    "result": {
        "n": 1,
        "ok": 1,
        "deletedCount": 1
    }
}