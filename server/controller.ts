import model from './model';

class Controller {

    constructor() { }


    //Select
    getVehicle() {
        return model.find({});
    }

    select(req, res) {
        this.getVehicle()
            .then(vehicle => res.status(200).json({ 'result': vehicle }))
            .catch(err => res.status(400).json({ 'result': err }));
    }


    //Selectone
    getVehicleByID(id) {
        return model.find(id);
    }

    selectOne(req, res) {
        const id = { _id: req.params.id }

        this.getVehicleByID(id)
            .then(vehicle => res.status(200).json({ 'result': vehicle }))
            .catch(err => res.status(400).json({ 'result': err }));
    }

    //Delete
    deleteByID(id) {
        return model.deleteOne(id);
    }

    delete(req, res) {
        const id = { _id: req.params.id }

        this.deleteByID(id)
            .then(vehicle => res.status(200).json({ 'result': vehicle }))
            .catch(err => res.status(400).json({ 'result': err }));
    }


    //Update
    updateVehicle(id, data) {
        return model.findOneAndUpdate(id, data);
    }

    update(req, res) {
        const id = { _id: req.params.id }
        const vehicle = req.body;

        this.updateVehicle(id, vehicle)
            .then(vehicle => res.status(200).json({ 'result': vehicle }))
            .catch(err => res.status(400).json({ 'result': err }));
    }


    //Insert
    createVehicle(data) {
        return model.create(data);
    }

    insert(req, res) {
        const vehicle = req.body;

        this.createVehicle(vehicle)
            .then(vehicle => res.status(200).json({ 'result': vehicle }))
            .catch(err => res.status(400).json({ 'result': err }));
    }


    //Selecione Placa
    getAllPlate(id, placa) {
        return model.find(id, placa);
    }

}

export default Controller;