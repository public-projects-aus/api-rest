 
import * as mongoose from 'mongoose';

const VehicleSchema = new mongoose.Schema({

    placa :{type: String, required: true },
    marca :{type: String, required: true },
    modelo:{type: String, required: true },
    cor   :{type: String, required: true },

    revisoes:{

            dataRevisao:{type: String, required: true},
            valor       :{type: String, required: true}

        },

    anofabricacao:{type: String, required: true },

   

    
    createdAt: {type: Date, default: Date.now}
})

export default mongoose.model('Vehicle', VehicleSchema);