import * as mongoose from 'mongoose';

class DataBase{

    private dburl = 'mongodb://127.0.0.1/test-maqplan';
    private dbconnection;

    constructor(){ }

    createConnection(){
        mongoose.connect(this.dburl);
        this.logger(this.dburl);
    }

    logger(uri){
        this.dbconnection = mongoose.connection;
        this.dbconnection.on('connected', () => console.log("Database Successfully Connected!") );
        this.dbconnection.on('error', error => console.error.bind(console, "error connecting to the database: " + error));
    }

}

export default DataBase;